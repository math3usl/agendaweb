<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Agenda - Cadastro de Contato</title>

<link href="css/jquery-ui.min.css" rel="stylesheet"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

</head>
<body>
	<c:import url="cabecalho.jsp"></c:import>
	
	<form action="mvc?logica=EditaContatoLogica&id=${requestScope.id}" method="post">
	Nome:
	<input type="text" name="nome" value="${requestScope.nome}"/> <br/> <br/>
	E-mail:
	<input type="text" name="email" value="${requestScope.email}"/> <br/> <br/>
	Endere�o:
	<input type="text" name="endereco" value="${requestScope.endereco}"/> <br/> <br/>
	Data de Nascimento:
	<senai:campoData id="dataNascimento" value="${requestScope.dataNascimento}"/> <br> <br>
	
	<input type="submit" name="Salvar"/>
	</form>
	
	<c:import url="rodape.jsp"></c:import>
</body>
</html>