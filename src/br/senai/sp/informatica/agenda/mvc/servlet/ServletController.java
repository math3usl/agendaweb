package br.senai.sp.informatica.agenda.mvc.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.senai.sp.informatica.agenda.mvc.logica.Logica;

@WebServlet("/mvc")
public class ServletController extends HttpServlet{

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// pega o par�metro l�gica da requisi��o
		String parametro = req.getParameter("logica");
		
		// monta o nome da classe de l�gica que deve ser executada
		String className = "br.senai.sp.informatica.agenda.mvc.logica." + parametro;
		
		try {
			// carrega a classe em mem�ria
			Class classe = Class.forName(className);
			
			// cria uma inst�ncia de l�gica
			Logica logica = (Logica) classe.newInstance();
			
			// cria a p�gina para redirecionar
			String pagina = logica.executa(req, res);
			
			// encaminhar o usu�rio
			req.getRequestDispatcher(pagina).forward(req, res);
		} catch (Exception e) {
			throw new ServletException("A l�gica de neg�cios causou uma exce��o", e);
		}
		
	}
}
