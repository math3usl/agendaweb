package br.senai.sp.informatica.agenda.mvc.logica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

public class EditaContatoLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// recupera o id da sess�o e converte de String para long
		long id = Long.parseLong(req.getParameter("id"));
		
		// recupera os dados do contato da sess�o
		String nome = req.getParameter("nome");
		String endereco = req.getParameter("endereco");
		String email = req.getParameter("email");
		String dataEmTexto = req.getParameter("dataNascimento");
		
		// converte a data de String para Calendar
		Calendar dataNascimento = null;
		try {
			// transforma a data de String para Date
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataEmTexto);
			// obt�m uma inst�ncia de Calendar
			dataNascimento = Calendar.getInstance();
			// define a dara do calend�rio utilizando o date
			dataNascimento.setTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		// obt�m uma inst�ncia de contato
		Contato contato = new Contato();
		// inicializar os atributos do contato com os atributos da sess�o
		contato.setId(id);
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(dataNascimento);
		
		// bot�m uma inst�ncia de ContatoDao
		ContatoDao dao = new ContatoDao();
		// salva o contato no banco de dados
		dao.salva(contato);
		// feedback para o usu�rio
		return "mvc?logica=ListaContatosLogica";		
	}
}