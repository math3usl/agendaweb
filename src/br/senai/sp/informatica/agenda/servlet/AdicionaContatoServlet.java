package br.senai.sp.informatica.agenda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

@WebServlet("/adicionaContato")
public class AdicionaContatoServlet extends HttpServlet {
	
	//sobrescreve o m�todo service
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		// obtem um objeto do tipo PrintWriter do response
		// �til para escrever mensagens do navegador do usu�rio
		PrintWriter out = res.getWriter();
		
		// pega os par�metros do formul�rio (request)
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String endereco = req.getParameter("endereco");
		String dataEmTexto = req.getParameter("dataNascimento");
		
		Calendar dataNascimento = null;
		
		try {
			// cria um objeto do tipo date e converte a data de string pra date
			Date data = new SimpleDateFormat("dd/MM/yyyy").parse(dataEmTexto);
			
			// obtem uma instancia do Calendar
			dataNascimento = Calendar.getInstance();
			
			// define a data do Calendar com a data do formul�rio
			dataNascimento.setTime(data);
		} catch (ParseException e) {
			// trata o ParseException
			out.println("Erro de convers�o de data !");
			return; // interrompe o m�todo service imediatamente
		}
		
		Contato contato = new Contato();
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(dataNascimento);
		
		// obtem uma instancia de ContatoDao e abre uma conexao com o banco de dados
		ContatoDao dao = new ContatoDao();
		
		//salva o contato no bando de dados
		dao.salva(contato);
		
		// feedback para o usu�rio
		// redireciona o usu�rio para a p�gina de feedback 
		RequestDispatcher dispatcher = req.getRequestDispatcher("/contato-adicionado.jsp");
		dispatcher.forward(req, res);
		
		/*out.print("<html>");
		out.print("<body>");
		out.print("Contato " + contato.getNome() + " salvo com sucesso!");
		out.print("</body>");
		out.print("</html>");*/
	}
}