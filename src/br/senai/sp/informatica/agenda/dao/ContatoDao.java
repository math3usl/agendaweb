package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.senai.sp.informatica.agenda.model.Contato;

public class ContatoDao {

	// atributo
	private Connection connection;

	// construtor
	public ContatoDao() {
		// estabelece uma conexao com o banco de dados
		connection = new ConnectionFactory().getConnection();
	}

	// salva
	public void salva(Contato contato) {
		String sql = null;
		
		// se o contato tem um id
		if (contato.getId() != null) {
			// fa�a um update
			sql = "UPDATE contato SET nome = ?, email = ?, endereco = ?, dataNascimento = ? WHERE id = ?";
		} else {
			// fa�a um insert
			sql = "INSERT INTO contato " + "(nome, email, endereco, dataNascimento) " + "VALUES (?, ?, ?, ?)";
		}

		try {
			// cria um PreparedStatement
			PreparedStatement stmt = connection.prepareStatement(sql);

			// cria os par�metros do PreparedStatement
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());

			stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
			
			if (contato.getId() != null) {
				stmt.setLong(5, contato.getId());
			}

			// executa o insert
			stmt.execute();

			// libera o recurso statement
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	// getLista
	public List<Contato> getLista() {

		try {
			// cria um ArrayList
			List<Contato> contatos = new ArrayList<>();

			// cria um PreparedStatement;
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contato");

			// executa o statement e guarda o resultado em um ResultSet
			ResultSet rs = stmt.executeQuery();

			// enquanto houver dados no resultset
			while (rs.next()) {
				// cria um contato com os dados do resultset
				Contato contato = new Contato();
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				// obt�m uma instancia de Calendar
				Calendar data = Calendar.getInstance();

				// define a data do calendar com a data do banco de dados
				data.setTime(rs.getDate("dataNascimento"));

				// define a data de nascimento do contato com o calendar
				contato.setDataNascimento(data);

				// adiciona o contato � lista de contatos
				contatos.add(contato);
			} // fim do while

			// fecha o resultset
			rs.close();

			// fecha o statement
			stmt.close();

			// retorna a lista de contato
			return contatos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void excluir(Contato contato) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM contato WHERE id = ?");
			stmt.setLong(1, contato.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
