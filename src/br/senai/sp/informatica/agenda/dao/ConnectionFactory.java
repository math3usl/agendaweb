package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() {
		try {
			// registra o driver jdbc
			Class.forName("com.mysql.jdbc.Driver");

			// retorna uma conex�o com o banco de dados gerada pelo DriverManager
			return DriverManager.getConnection("jdbc:mysql://localhost/agendat", "root", "root132");
		} catch (SQLException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}